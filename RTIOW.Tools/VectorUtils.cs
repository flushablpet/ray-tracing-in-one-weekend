﻿using System.Numerics;

namespace RTIOW.Tools;

public static class VectorUtils
{
    private static readonly Random rand = new();

    /// <summary>
    /// 8.1 - Get a random point inside a unit sphere.
    /// </summary>
    /// <returns></returns>
    public static Vector3 RandomInUnitSphere()
    {
        while (true)
        {
            var p = Random(-1, 1);
            if (p.LengthSquared() >= 1)
                continue;

            return p;
        }
    }

    /// <summary>
    /// Get a random point inside a unit hemisphere.
    /// </summary>
    /// <param name="normal"></param>
    /// <returns></returns>
    public static Vector3 RandomInHemisphere(Vector3 normal)
    {
        var unitSphere = RandomInUnitSphere();

        // In same hemisphere as normal if true
        return Vector3.Dot(unitSphere, normal) > 0 ? unitSphere : -unitSphere;
    }

    /// <summary>
    /// Generate a random float between 0 and 1.
    /// </summary>
    /// <returns></returns>
    public static float GetRandom() => rand.NextSingle();

    /// <summary>
    /// Get a random float between min and max.
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static float GetRandom(float min, float max) => min + (max - min) * rand.NextSingle();

    /// <summary>
    /// Get a random vector with x,y,z between 0-1.
    /// </summary>
    /// <returns></returns>
    public static Vector3 Random() => new(GetRandom(), GetRandom(), GetRandom());

    /// <summary>
    /// Get a random vector with x,y,z between min-max.
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static Vector3 Random(float min, float max) =>
        new(GetRandom(min, max), GetRandom(min, max), GetRandom(min, max));

    /// <summary>
    /// 8.5 - Lambertian reflection
    /// </summary>
    /// <returns></returns>
    public static Vector3 RandomUnitVector() => Vector3.Normalize(RandomInUnitSphere());

    /// <summary>
    /// Clamp value between min and max.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="t"></param>
    /// <returns></returns>
    public static float Clamp(float x, (float min, float max) t)
    {
        if (x < t.min)
            return t.min;
        if (x > t.max)
            return t.max;
        return x;
    }

    const float S = 1e-8f;

    /// <summary>
    /// Check if vector fields are close to zero
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static bool NearZero(this Vector3 v) => (v.X < S) && (v.Y < S) && v.Z < S;

    /// <summary>
    /// Create reflection vector.
    /// </summary>
    /// <param name="v"></param>
    /// <param name="u"></param>
    /// <returns></returns>
    public static Vector3 Reflect(Vector3 v, Vector3 u) => v - 2 * Vector3.Dot(v, u) * u;

    /// <summary>
    /// Create refraction vector.
    /// </summary>
    /// <param name="uv">Unit ray direction</param>
    /// <param name="n">Normal</param>
    /// <param name="etaOverEtaPrime"></param>
    /// <returns></returns>
    public static Vector3 Refract(Vector3 uv, Vector3 n, float etaOverEtaPrime)
    {
        var cosTheta = Math.Min(Vector3.Dot(-uv, n), 1);
        var rPerp = etaOverEtaPrime * (uv + cosTheta * n);
        var rParallel = (float)-Math.Sqrt(Math.Abs(1 - rPerp.LengthSquared())) * n;
        return rPerp + rParallel;
    }

    public static Vector3 RandomInUnitDisc()
    {
        while (true)
        {
            var p = new Vector3(GetRandom(-1, 1), GetRandom(-1, 1), 0);
            if (p.LengthSquared() >= 1)
                continue;

            return p;
        }
    }
}
