﻿using RTIOW.Properties.Materials;
using RTIOW.Properties.Scene.Shapes;

namespace RTIOW.Properties.Scene;

/// <inheritdoc/>
public sealed class SimpleScene : SceneBase
{
    /// <summary>
    /// Creates a basic scene with a sphere and background.
    /// </summary>
    public SimpleScene()
    {
        CreateAScene();
    }

    /// <inheritdoc/>
    public override void CreateAScene()
    {
        var matRed = new DefaultMaterial(new Color(0.9f, 0, 0));
        var matGreen = new DefaultMaterial(new Color(0, 1, 0));
        Items.Add(new Sphere(new Point(0, 0, -1), 0.5f, matRed));
        Items.Add(new Sphere(new Point(0, -100.5f, -1), 100, matGreen));
    }
}
