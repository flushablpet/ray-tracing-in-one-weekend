﻿using System.Numerics;
using RTIOW.Properties.Light;
using RTIOW.Properties.Settings;
using RTIOW.Tools;

namespace RTIOW.Properties.Scene;

/// <summary>
/// A camera instance provides a view into a scene
/// </summary>
public sealed class Camera : ICamera
{
    private const float VHEIGHT_COEFF = 2.0F;

    //private Point origin = new(0, 0, -1);
    private Point lowerLeftCorner = default;
    private Point horizontal;
    private Point vertical;

    // Camera x horiz axis
    private Point u = default;

    // Camera axis y vertical axis
    private Point v = default;

    // Camera z axis. +w looks behind, -w looks at target.
    private Point w = default;

    private (float height, float width) vDims;

    /// <summary>
    /// Initializes a new instance of a Camera object.
    /// </summary>
    /// <param name="settings">Options collection</param>
    public Camera(RaytracerSettings settings)
    {
        FocalDistance = settings.FocalDistance;
        Aperture = settings.Aperture;
        AspectRatio = settings.AspectRatio;
        vDims = Dims(settings.VerticalFOV);
        Console.WriteLine("Camera initialized.");
    }

    private (float height, float width) Dims(float vFov)
    {
        var h = (float)Math.Tan(vFov * Math.PI / 180 / 2);
        var vHeight = VHEIGHT_COEFF * h;
        var vWidth = AspectRatio * vHeight;
        return (vHeight, vWidth);
    }

    /// <inheritdoc/>
    public Point Origin { get; set; }

    /// <inheritdoc/>
    public Point LookAt { get; set; }

    /// <inheritdoc/>
    public Point YAxis { get; set; }

    /// <inheritdoc/>
    public float FocalDistance { get; init; }

    /// <inheritdoc/>
    public float AspectRatio { get; init; }

    /// <inheritdoc/>
    public float LensRadius
    {
        get => Aperture / 2;
    }

    /// <inheritdoc/>
    public float Aperture { get; init; }

    /// <inheritdoc/>
    public Ray GetRay(float s, float t)
    {
        var rd = LensRadius * VectorUtils.RandomInUnitDisc();
        var offset = u * rd.X + v * rd.Y;

        var originOffset = Origin + offset;
        var dir = lowerLeftCorner + s * horizontal + t * vertical - Origin - offset;
        return new Ray(originOffset, dir);
    }

    /// <inheritdoc/>
    public void Orient(Point origin, Point lookAt)
    {
        Origin = origin;
        LookAt = lookAt;
        YAxis = new Vector3(0, 1, 0);
        w = Point.Normalize(Origin - LookAt);
        u = Point.Normalize(Point.Cross(YAxis, w));
        v = Point.Cross(w, u);

        horizontal = FocalDistance * vDims.width * u;
        vertical = FocalDistance * vDims.height * v;
        lowerLeftCorner = Origin - horizontal / 2 - vertical / 2 - FocalDistance * w;
    }
}
