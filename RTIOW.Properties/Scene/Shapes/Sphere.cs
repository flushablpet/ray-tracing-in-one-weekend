﻿using RTIOW.Properties.Light;
using RTIOW.Properties.Materials;
using System.Numerics;

namespace RTIOW.Properties.Scene.Shapes;

internal sealed class Sphere : ShapeBase
{
    public Sphere(Point center, float radius, MaterialBase m) =>
        (Center, Radius, Material) = (center, radius, m);

    public MaterialBase Material { get; }

    /// <summary>
    /// Center of sphere
    /// </summary>
    public Point Center { get; init; }

    /// <summary>
    /// Radius of sphere.
    /// </summary>
    public float Radius { get; init; }

    /// <inheritdoc/>
    public override bool CollisionCheck(Ray r, (float min, float max) t, ref HitRecord hit)
    {
        var oc = r.Origin - Center;
        // Dot(a, a) is equal to length^2 of a
        var a = r.Direction.LengthSquared();
        var half_b = Vector3.Dot(oc, r.Direction);
        var c = oc.LengthSquared() - Radius * Radius;

        var discriminant = half_b * half_b - a * c;
        if (discriminant < 0)
        {
            return false;
        }

        var discrimSq = (float)Math.Sqrt(discriminant);
        // Find nearest root in range
        var root = (-half_b - discrimSq) / a;
        if (RootInRange(root, t))
        {
            root = (-half_b + discrimSq) / a;
            if (RootInRange(root, t))
            {
                return false;
            }
        }

        hit.T = root;
        hit.Point = r.At(root);
        hit.SetFaceNormal(r, (hit.Point - Center) / Radius);
        hit.Material = Material;
        return true;

        static bool RootInRange(float root, (float min, float max) t) =>
            root < t.min || root > t.max;
    }
}
