﻿using RTIOW.Properties.Light;

namespace RTIOW.Properties.Scene.Shapes;

/// <summary>
/// An abstract definition of a shape that a ray cast into a scene may collide with.
/// </summary>
public abstract class ShapeBase
{
    /// <summary>
    /// If a ray indicents this object, create a hit record.
    /// </summary>
    /// <param name="r"></param>
    /// <param name="t"></param>
    /// <param name="hit"></param>
    /// <returns></returns>
    public abstract bool CollisionCheck(Ray r, (float min, float max) t, ref HitRecord hit);
}
