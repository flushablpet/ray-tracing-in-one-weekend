﻿using RTIOW.Properties.Light;
using RTIOW.Properties.Scene.Shapes;

namespace RTIOW.Properties.Scene;

/// <summary>
/// A scene holds all the shapes contained within
/// </summary>
public abstract class SceneBase : IScene
{
    private readonly List<ShapeBase> items = new();

    /// <summary>
    /// Create an empty scene.
    /// </summary>
    public SceneBase() { }

    /// <summary>
    /// Members of scene
    /// </summary>
    public List<ShapeBase> Items
    {
        get { return items; }
    }

    /// <summary>
    /// Clear all items from the scene.
    /// </summary>
    public void Clear()
    {
        items.Clear();
    }

    /// <summary>
    /// Add an item to the scene.
    /// </summary>
    /// <param name="shape"></param>
    public void Add(ShapeBase shape)
    {
        items.Add(shape);
    }

    /// <summary>
    /// Check for ray collisions with scene objects.
    /// </summary>
    /// <param name="r"></param>
    /// <param name="t"></param>
    /// <param name="hit"></param>
    /// <returns></returns>
    public bool CollisionCheck(Ray r, (float min, float max) t, out HitRecord hit)
    {
        hit = new HitRecord();
        var anyHit = false;
        var (min, max) = t;
        foreach (var item in items)
        {
            if (item.CollisionCheck(r, (min, max), ref hit))
            {
                anyHit = true;
                max = hit.T;
            }
        }
        return anyHit;
    }

    /// <summary>
    /// Build a scene object
    /// </summary>
    public abstract void CreateAScene();
}
