﻿using RTIOW.Properties.Materials;
using RTIOW.Properties.Scene.Shapes;

namespace RTIOW.Properties.Scene;
internal sealed class BasicScene : SceneBase
{
    /// <summary>
    /// Three spheres on a ground surface
    /// </summary>
    public BasicScene()
    {
        CreateAScene();
    }

    public override void CreateAScene()
    {
        var ground = new DefaultMaterial(new Color(1f, 0.0f, 0.0f));
        var glass = new DielectricMaterial(new Color(0.65f, 0.4f, 0.3f), 1.5f);
        var metal = new MetalMaterial(new Color(0.8f, 0.5f, 0.15f), 0.15f);
        var globe = new Sphere(new Point(0, -100, -1), 1, ground);
        var metalSphere = new Sphere(new Point(-1, 0, -1), 0.5f, metal);
        var glassSphere = new Sphere(new Point(1, 0, -1), -0.45f, glass);
        Items.AddRange(new Sphere[] { metalSphere, globe, glassSphere });
    }
}
