﻿using RTIOW.Properties.Materials;
using RTIOW.Properties.Scene.Shapes;
using RTIOW.Tools;

namespace RTIOW.Properties.Scene;

/// <summary>
/// Scene with a lot of items, slow render time.
/// </summary>
public class ComplexScene : SceneBase
{
    /// <summary>
    /// Creates a scene containing many objects of varying materials.
    /// </summary>
    public ComplexScene()
    {
        CreateAScene();
    }

    /// <inheritdoc/>
    public override void CreateAScene()
    {
        var groundMaterial = new DefaultMaterial(new Color(0.5f, 0.5f, 0.5f));
        Items.Add(new Sphere(new Point(0, -1000, 0), 1000, groundMaterial));

        var fixedPoint = new Point(4, 0.2f, 0);
        for (var a = -11; a < 11; a++)
        {
            for (var b = -11; b < 11; b++)
            {
                var matSeed = VectorUtils.GetRandom();
                var center = new Point(
                    a + 0.9f * VectorUtils.GetRandom(),
                    0.2f,
                    b + 0.9f * VectorUtils.GetRandom()
                );

                if ((center - fixedPoint).Length() <= 0.9f)
                {
                    continue;
                }

                var item = matSeed switch
                {
                    < 0.8f
                      => new Sphere(
                          center,
                          0.2f,
                          new DefaultMaterial(VectorUtils.Random() * VectorUtils.Random())
                      ),
                    < 0.95f
                      => new Sphere(
                          center,
                          0.2f,
                          new MetalMaterial(
                              VectorUtils.Random(0.5f, 1),
                              VectorUtils.GetRandom(0, 0.5f)
                          )
                      ),
                    _
                      => new Sphere(
                          center,
                          0.2f,
                          new DielectricMaterial(new Color(0.2f, 1, 0.5f), 1.5f)
                      ),
                };
                Items.Add(item);
            }
        }

        // Add some chonks
        var mat1 = new DielectricMaterial(new Color(0, 0, 0), 1.35f);
        Items.Add(new Sphere(new Point(0, 1, 0), 1.0f, mat1));

        var mat2 = new DefaultMaterial(new Color(0.4f, 0.2f, 0.1f));
        Items.Add(new Sphere(new Point(-4, 1, 0), 1.0f, mat2));

        var mat3 = new MetalMaterial(new Color(0.7f, 0.6f, 0.5f));
        Items.Add(new Sphere(new Point(4, 1, 0), 1.0f, mat3));
    }
}
