﻿using RTIOW.Properties.Light;

namespace RTIOW.Properties.Scene;

/// <summary>
/// Camera interface
/// </summary>
public interface ICamera
{
    /// <summary>
    /// Camera's position in 3D space.
    /// </summary>
    Point Origin { get; set; }

    /// <summary>
    /// Point to aim the camera at.
    /// </summary>
    Point LookAt { get; set; }

    /// <summary>
    /// Impacts the amount of light received.
    /// </summary>
    float Aperture { get; init; }

    /// <summary>
    /// Camera's focal distance to target.
    /// </summary>
    float FocalDistance { get; init; }

    /// <summary>
    /// W:H aspect ratio of camera view
    /// </summary>
    float AspectRatio { get; init; }

    /// <summary>
    /// Size of lens in relation to aperture
    /// </summary>
    float LensRadius { get; }

    /// <summary>
    /// Generates a ray from the camera to the given coordinates.
    /// </summary>
    /// <param name="s"></param>
    /// <param name="t"></param>
    /// <returns></returns>
    Ray GetRay(float s, float t);

    /// <summary>
    /// Position the camera view.
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="lookAt"></param>
    void Orient(Point origin, Point lookAt);
}
