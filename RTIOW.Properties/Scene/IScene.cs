﻿using RTIOW.Properties.Light;
using RTIOW.Properties.Scene.Shapes;

namespace RTIOW.Properties.Scene;

/// <summary>
/// Interface for a scene
/// </summary>
public interface IScene
{
    /// <summary>
    /// Remove all scene members
    /// </summary>
    void Clear();

    /// <summary>
    /// Add a member to the scene
    /// </summary>
    /// <param name="shape"></param>
    void Add(ShapeBase shape);

    /// <summary>
    /// Test for ray collision with scene members.
    /// </summary>
    /// <param name="r"></param>
    /// <param name="t"></param>
    /// <param name="hit"></param>
    /// <returns></returns>
    bool CollisionCheck(Ray r, (float min, float max) t, out HitRecord hit);
}
