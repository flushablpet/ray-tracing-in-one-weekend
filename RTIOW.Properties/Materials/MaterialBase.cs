﻿using RTIOW.Properties.Light;

namespace RTIOW.Properties.Materials;

/// <summary>
/// Material properties.
/// </summary>
public abstract class MaterialBase
{
    /// <summary>
    /// Creates a new instance of a material
    /// </summary>
    /// <param name="color"></param>
    public MaterialBase(Color color)
    {
        Albedo = color;
    }

    /// <summary>
    /// Determines the diffuse light reflected from the material.
    /// Black is a blackbody material that absorbs all incoming rays. White reflects all incoming rays.
    /// </summary>
    public Color Albedo { get; }

    /// <summary>
    /// Determines if the incoming ray needs to be scattered or reflected.
    /// </summary>
    /// <param name="r">Incoming ray</param>
    /// <param name="hit">Hit record</param>
    /// <param name="attenuationColor">Color of attenuated ray</param>
    /// <param name="scattered">Generated scattered ray</param>
    /// <returns></returns>
    public abstract bool Scatter(
        Ray r,
        HitRecord hit,
        out Color attenuationColor,
        out Ray scattered
    );
}
