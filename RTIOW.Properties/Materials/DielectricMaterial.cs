﻿using RTIOW.Properties.Light;
using RTIOW.Tools;
using System.Numerics;

namespace RTIOW.Properties.Materials;

/// <summary>
/// Glass-type material.
/// </summary>
public sealed class DielectricMaterial : MaterialBase
{
    /// <summary>
    /// Creates a glass type material.
    /// </summary>
    /// <param name="color">Vector3 color</param>
    /// <param name="ior">Indicence of Reflection</param>
    public DielectricMaterial(Color color, float ior) : base(color)
    {
        IOR = ior;
    }

    /// <summary>
    /// Incidence of Reflection
    /// </summary>
    public float IOR { get; }

    /// <summary>
    /// Scatter ray accounting for refraction and reflectance.
    /// </summary>
    /// <param name="ray"></param>
    /// <param name="hit"></param>
    /// <param name="attenuationColor"></param>
    /// <param name="scattered"></param>
    /// <returns></returns>
    public override bool Scatter(
        Ray ray,
        HitRecord hit,
        out Color attenuationColor,
        out Ray scattered
    )
    {
        attenuationColor = new Color(1, 1, 1);
        var refractionRatio = hit.IsOutwardFacing ? 1.0f / IOR : IOR;
        var unitDirection = Vector3.Normalize(ray.Direction);
        var cosTheta = Math.Min(Vector3.Dot(-unitDirection, hit.Normal), 1.0f);
        var sinTheta = Math.Sqrt(1.0f - cosTheta * cosTheta);

        var cannotRefract = refractionRatio * sinTheta > 1.0f;
        if (
            cannotRefract
            || SchlickReflectance(cosTheta, refractionRatio) > VectorUtils.GetRandom()
        )
        {
            scattered = new Ray(hit.Point, VectorUtils.Reflect(unitDirection, hit.Normal));
            return true;
        }

        scattered = new Ray(
            hit.Point,
            VectorUtils.Refract(unitDirection, hit.Normal, refractionRatio)
        );
        return true;
    }

    private static float SchlickReflectance(float cos, float idx)
    {
        var r0 = (1 - idx) / (1 + idx);
        r0 *= r0;
        return r0 + (1 - r0) * (float)Math.Pow(1 - cos, 5);
    }
}
