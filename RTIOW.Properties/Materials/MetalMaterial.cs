﻿using RTIOW.Properties.Light;
using RTIOW.Tools;
using System.Numerics;

namespace RTIOW.Properties.Materials;

/// <summary>
/// Metallic form material.
/// </summary>
public sealed class MetalMaterial : MaterialBase
{
    /// <summary>
    /// Randomizes the direction of reflected rays.
    /// The greater the value, the less polished the metal reflection will be.
    /// </summary>
    private readonly float fuzz;

    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    /// <param name="color"></param>
    /// <param name="fuzz"></param>
    public MetalMaterial(Color color, float fuzz = 0.3f) : base(color)
    {
        this.fuzz = fuzz < 1 ? fuzz : 1;
    }

    /// <inheritdoc/>
    public override bool Scatter(
        Ray r,
        HitRecord hit,
        out Color attenuationColor,
        out Ray scattered
    )
    {
        attenuationColor = Albedo;
        var reflected = VectorUtils.Reflect(Vector3.Normalize(r.Direction), hit.Normal);
        scattered = new Ray(hit.Point, reflected + fuzz * VectorUtils.RandomInUnitSphere());

        return Vector3.Dot(scattered.Direction, hit.Normal) > 0;
    }
}
