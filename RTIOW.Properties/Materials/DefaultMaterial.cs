﻿using RTIOW.Properties.Light;
using RTIOW.Tools;

namespace RTIOW.Properties.Materials;

/// <summary>
/// The default Lambertian style material. Fuzzy warm and diffuse.
/// </summary>
public class DefaultMaterial : MaterialBase
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    /// <param name="color"></param>
    public DefaultMaterial(Color color) : base(color) { }

    /// <inheritdoc/>
    public override bool Scatter(Ray r, HitRecord hit, out Color color, out Ray scattered)
    {
        var scatterDirection = hit.Normal + VectorUtils.RandomUnitVector();
        if (scatterDirection.NearZero())
        {
            scatterDirection = hit.Normal;
        }

        scattered = new Ray(hit.Point, scatterDirection);
        color = Albedo;
        return true;
    }
}
