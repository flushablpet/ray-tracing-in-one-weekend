﻿// https://raytracing.github.io/books/RayTracingInOneWeekend.html#overview

namespace RTIOW.Properties.Settings;

/// <summary>
/// Main settings file loaded from appsettings.json
/// </summary>
public class RaytracerSettings
{
    public float VerticalFOV { get; init; } = 30;
    public float AspectRatio { get; init; } = 1.5f;

    public float Aperture { get; init; } = 0.1f;

    public float FocalDistance { get; init; }

    /// <summary>
    /// Image width.
    /// </summary>
    public int Width { get; init; } = 400;

    /// <summary>
    /// Image height.
    /// </summary>
    public int Height
    {
        get => (int)Math.Floor(Width / AspectRatio);
    }

    /// <summary>
    /// Output filename minus extension.
    /// </summary>
    public string DefaultFilename { get; init; } = "scene";

    /// <summary>
    /// Number of subsamples per pixel.
    /// </summary>
    public int SamplesPerPixel { get; init; }

    /// <summary>
    /// Max number of ray bounces per ray cast.
    /// </summary>
    public int MaxRayBounces { get; init; }
}
