﻿namespace RTIOW.Properties.Light;

/// <summary>
/// Defines a ray cast from an origin point in a given direction.
/// </summary>
public readonly struct Ray
{
    /// <summary>
    /// Creates a new instance of a Ray
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="direction"></param>
    public Ray(Point origin, Point direction)
    {
        Origin = origin;
        Direction = direction;
    }

    /// <summary>
    /// Ray origin point
    /// </summary>
    public readonly Point Origin { get; init; }

    /// <summary>
    /// Direction of emission
    /// </summary>
    public readonly Point Direction { get; init; }

    /// <summary>
    /// Extend the ray along its path over time.
    /// </summary>
    /// <param name="t">Unit of time</param>
    /// <returns></returns>
    public Point At(float t) => Origin + Point.Multiply(t, Direction);
}
