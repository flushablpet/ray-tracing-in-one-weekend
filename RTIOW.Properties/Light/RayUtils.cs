﻿using RTIOW.Properties.Scene;

namespace RTIOW.Properties.Light;

/// <summary>
/// Static helper functions for Ray handling.
/// </summary>
public static class RayUtils
{
    /// <summary>
    /// 8.4 - Shadow Acne
    /// </summary>
    const float MIN = 0.001f,
        MAX = float.MaxValue;

    /// <summary>
    /// Create a linear gradient between two vector values in given increments
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <param name="t"></param>
    /// <returns></returns>
    public static Color Gradient(Color from, Color to, float t) => Color.Lerp(from, to, t);

    /// <summary>
    /// Sets the pixel color. Sec. 4.2.
    /// </summary>
    /// <param name="r"></param>
    /// <param name="scene"></param>
    /// <param name="bounceLimit"></param>
    /// <returns></returns>
    public static Color RayColor(in Ray r, IScene scene, int bounceLimit)
    {
        // If limit reached, gather no more light
        if (bounceLimit <= 0)
        {
            return new Color(0, 0, 0);
        }

        if (scene.CollisionCheck(r, (MIN, MAX), out HitRecord hit))
        {
            if (hit.Material.Scatter(r, hit, out Color color, out Ray scattered))
            {
                return color * RayColor(scattered, scene, --bounceLimit);
            }
            return new Color(0, 0, 0);
        }

        var unitDir = Color.Normalize(r.Direction);
        var t = 0.5f * (unitDir.Y + 1.0f);

        return Gradient(new Color(1, 1, 1), new Color(0.2f, 0.3f, 0.8f), t);
    }
}
