﻿using RTIOW.Properties.Materials;

namespace RTIOW.Properties.Light;

/// <summary>
/// Data when a light ray intersects with an object
/// </summary>
public struct HitRecord
{
    /// <summary>
    /// Hit point
    /// </summary>
    public Point Point { get; set; }

    /// <summary>
    /// Normal of hit vector
    /// </summary>
    public Point Normal { get; set; }

    /// <summary>
    /// Material for scatter calculations
    /// </summary>
    public MaterialBase Material { get; set; }

    /// <summary>
    /// Hit value
    /// </summary>
    public float T { get; set; }

    /// <summary>
    /// Normal direction
    /// </summary>
    public bool IsOutwardFacing { get; set; }

    /// <summary>
    /// Flips the direction of a face normal vector.
    /// </summary>
    /// <param name="r"></param>
    /// <param name="outwardNormal"></param>
    public void SetFaceNormal(Ray r, Point outwardNormal)
    {
        IsOutwardFacing = Point.Dot(r.Direction, outwardNormal) < 0;
        Normal = IsOutwardFacing ? outwardNormal : -outwardNormal;
    }
}
