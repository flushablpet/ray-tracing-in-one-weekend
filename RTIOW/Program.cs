﻿// https://raytracing.github.io/books/RayTracingInOneWeekend.html#overview
global using Color = System.Numerics.Vector3;
global using Point = System.Numerics.Vector3;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using RTIAW.Rendering;
using RTIOW.Properties.Settings;
using RTIOW.Rendering;
using RTIOW.Properties.Scene;

IConfiguration config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json", optional: false)
    .Build();
var settings = config.GetRequiredSection("RaytracerSettings").Get<RaytracerSettings>();

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration(
        b =>
        {
            b.Sources.Clear();
            b.AddJsonFile("appsettings.json", optional: false);
            b.Build();
        }
    ).Build();

Memory<Color> memory = new Color[settings.Width * settings.Height];
var render = new Render(new ComplexScene(), settings);
var camera = new Camera(settings);
camera.Orient(new Point(13, 2, 3), new Point(0, 0, 0));

render.Go(camera, memory);
await FormatPNG.SaveAsPng(memory, settings);
