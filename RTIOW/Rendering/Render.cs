﻿using RTIOW.Properties.Light;
using RTIOW.Properties.Scene;
using RTIOW.Properties.Settings;
using RTIOW.Tools;

namespace RTIAW.Rendering;

internal class Render
{
    private readonly IScene scene;
    private readonly RaytracerSettings settings;
    private readonly int imageHeight,
        imageWidth,
        samplesPerPixel,
        maxBounces;

    public Render(IScene scene, RaytracerSettings settings)
    {
        this.scene = scene;
        this.settings = settings;
        imageHeight = settings.Height;
        imageWidth = settings.Width;
        samplesPerPixel = settings.SamplesPerPixel;
        maxBounces = settings.MaxRayBounces;
        Console.WriteLine("Configured render settings.");
    }

    /// <summary>
    /// Render the given scene to the memory span.
    /// </summary>
    /// <param name="scene">Scene data</param>
    /// <param name="mem">Memory span</param>
    public void Go(ICamera cam, Memory<Color> mem)
    {
        Console.WriteLine("Beginning render.");
        // Render row pixels in parallel
        for (var y = imageHeight - 1; y >= 0; y--)
        {
            Console.WriteLine($"Rendering row {y}.");
            Parallel.For(0, imageWidth, x => RenderPixel(x, y));
        }

        Console.WriteLine(
            $"Rendered {imageHeight * imageWidth} pixels, {samplesPerPixel} subsamples."
        );

        void RenderPixel(int x, int y)
        {
            var span = mem.Span;
            Color pixelColor = new(0, 0, 0);
            for (var sample = 0; sample < samplesPerPixel; sample++)
            {
                float s = (x + VectorUtils.GetRandom()) / (imageWidth - 1);
                float t = (y + VectorUtils.GetRandom()) / (imageHeight - 1);
                var ray = cam.GetRay(s, t);
                pixelColor += RayUtils.RayColor(in ray, scene, maxBounces);
            }
            span[(y * imageWidth) + x] = pixelColor;
        }
    }
}
