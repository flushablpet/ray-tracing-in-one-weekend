﻿using RTIOW.Properties.Settings;
using RTIOW.Tools;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace RTIOW.Rendering;
internal static class FormatPNG
{
    private static readonly (float cMin, float cMax) clamp = (0, 0.999f);
    private const int BYTESPACE = 256;

    private static void EncodePixel(Color dataPixel, int samples, ref Rgb24 imgPixel)
    {
        float scale = 1.0f / samples;

        var r = Gamma(scale, dataPixel.X);
        var g = Gamma(scale, dataPixel.Y);
        var b = Gamma(scale, dataPixel.Z);

        imgPixel.R = ClampColor(r);
        imgPixel.G = ClampColor(g);
        imgPixel.B = ClampColor(b);

        // Raise colors to 1/gamma, where gamma = 2
        static float Gamma(float scale, float color) => (float)Math.Sqrt(scale * color);

        static byte ClampColor(float color) =>
            (byte)(BYTESPACE * VectorUtils.Clamp(color, clamp));
    }

    public static async Task SaveAsPng(ReadOnlyMemory<Color> data, RaytracerSettings settings)
    {
        var file = Path.ChangeExtension(settings.DefaultFilename, ".png");
        Console.WriteLine($"Writing output to {file}");

        using var img = new Image<Rgb24>(settings.Width, settings.Height);
        var dim = (settings.Width * settings.Height) - 1;

        img.ProcessPixelRows(
            pacc =>
            {
                for (var y = 0; y < pacc.Height; y++)
                {
                    var row = pacc.GetRowSpan(y);
                    for (var x = 0; x < row.Length; x++)
                    {
                        ref Rgb24 pixel = ref row[x];

                        // Render data is mirror inverted, so flip it
                        var dataPixel = data.Span[dim - (y * settings.Width + x)];
                        EncodePixel(dataPixel, settings.SamplesPerPixel, ref pixel);
                    }
                }
            }
        );

        await img.SaveAsPngAsync(file);
        Console.WriteLine($"Output written to {file}");
    }
}
