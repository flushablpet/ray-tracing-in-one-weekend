﻿using RTIOW.Properties.Settings;
using RTIOW.Tools;
using System.Text;

namespace RTIOW.Rendering;

/// <summary>
/// Generates a PPM format data file containing image data.
/// </summary>
internal static class FormatPPM
{
    private const string EXT = ".ppm";
    private const int BYTESPACE = 256;
    private const int COLOR_DEPTH = 255;
    private static readonly (float cMin, float cMax) clamp = (0, 0.999f);

    public static byte[] EncodePixel(Color pixel, int samples)
    {
        float scale = 1.0f / samples;

        var r = Gamma(scale, pixel.X);
        var g = Gamma(scale, pixel.Y);
        var b = Gamma(scale, pixel.Z);

        return Encoding.UTF8.GetBytes($"{ClampColor(r)} {ClampColor(g)} {ClampColor(b)}\n");

        // Raise colors to 1/gamma, where gamma = 2
        static float Gamma(float scale, float color) => (float)Math.Sqrt(scale * color);

        static int ClampColor(float color) => (int)(BYTESPACE * VectorUtils.Clamp(color, clamp));
    }

    public static byte[] EncodeHeader(RaytracerSettings s) =>
        Encoding.UTF8.GetBytes($"P3\n{s.Width} {s.Height}\n{COLOR_DEPTH}\n");

    /// <summary>
    /// Save the image data to PPM format.
    /// </summary>
    /// <param name="data">Image data</param>
    /// <param name="s">Image settings</param>
    /// <param name="samples">Samples per pixel</param>
    /// <returns></returns>
    public static async Task SaveAs(ReadOnlyMemory<Color> data, RaytracerSettings s)
    {
        var file = Path.ChangeExtension(s.DefaultFilename, EXT);
        using var fs = new FileStream(file, FileMode.Create);
        await fs.WriteAsync(EncodeHeader(s));

        for (var i = (s.Height * s.Width) - 1; i >= 0; i--)
        {
            await fs.WriteAsync(EncodePixel(data.Span[i], s.SamplesPerPixel));
        }

        Console.WriteLine($"Render data saved to {file}");
    }

    public static async Task Test(RaytracerSettings s)
    {
        using var fs = new FileStream("test.ppm", FileMode.Create);
        await fs.WriteAsync(EncodeHeader(s));
        for (var y = s.Height - 1; y >= 0; y--)
        {
            for (var x = 0; x < s.Width; x++)
            {
                var r = (float)x / (s.Width - 1);
                var g = (float)y / (s.Height - 1);
                float b = 0.25f;

                await fs.WriteAsync(EncodePixel(new Color(r, g, b), s.SamplesPerPixel));
            }
        }
    }
}
